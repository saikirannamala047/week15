package com.Week15_GL_oto.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.Week15_GL_oto.entity.Person;

public interface RepositoryForPerson extends JpaRepository<Person, Integer>{  
}
