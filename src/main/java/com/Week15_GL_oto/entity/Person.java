package com.Week15_GL_oto.entity;

import lombok.Getter;  
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Person {  
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "id_card_id")
    private ID idCard;

    public Person(String name, ID idCard) {
        this.name = name;
        this.idCard = idCard;
      
    }
}